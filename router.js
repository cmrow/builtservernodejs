const people = [{
        "names": "Angie",
        "lastName": "Cortes",
        "address": "Cr 76 # 5-33",
        "phone": "3133049099",
        "email": "angiecortes29@gmail.com",
        "hobbies": ['Jugar futbol', 'video juegos', 'leer'],
        "favoriteColors": ['azul todos', 'negro', 'magenta']
    },
    {
        "names": "Adiela",
        "lastName": "lopez peña",
        "address": "Cr 95a #138-65",
        "phone": "9303010",
        "email": "lumos.solem15@gmail.com",
        "hobbies": ['cantar', 'cazar moscas'],
        "favoriteColors": ['rosa', 'negro', 'purpura']
    },
    {
        "names": "Uriel",
        "lastName": "Ardila Gomez",
        "address": "tr 14 m bis # 68a - 21 sur",
        "phone": "7682545",
        "email": "urielardila@hotmail.com",
        "hobbies": ['leer', 'baloncesto', 'programar'],
        "favoriteColors": ['rojo', 'verde', 'negro']
    },
    {
        "names": "Juan Alirio",
        "lastName": "Gomez",
        "address": "Carretera central del norte km 17",
        "phone": "6763097",
        "email": "juanaliriogomez@gmail.com",
        "hobbies": ['jardineria', 'artesanias', 'manualidades'],
        "favoriteColors": ['no prioridad por algún color']
    }

];


const http = require('http'),
    fs = require('fs'),
    path = require('path'),
    urls = [
        { route: "", out: "./index.html" },
        { route: "angie", out: "./angie.html" },
        { route: "adiela", out: "./adiela.html" },
        { route: "uriel", out: "./uriel.html" },
        { route: "juan", out: "./juan.html" },
    ];


console.log("antes de crear server");
http.createServer((request, response) => {
    // console.log("despues de crear server");
    // console.log('request.url: ' + request.url);
    // if (request.url == '/') {

    // }
    let rout = path.basename(request.url);
    // let rout = request.url;
    console.log("rout: " + rout);
    urls.forEach((item) => {
        if (item.route == rout) {
            // console.log("item.route: " + item.route);
            // console.log(rout);
            // console.log('item.out' + item.out);
            fs.readFile(item.out, (er, data) => {
                htmlText = data.toString();
                // console.log('htmlText :' + htmlText);
                let validacionInerpolacion = htmlText.match(/[^\{\}]+(?=\})/g);
                // console.log('validacionInerpolacion : ' + validacionInerpolacion[0]);
                // console.log('validacionInerpolacion.length : ' + validacionInerpolacion.length);
                console.log(rout);
                var person;
                if (rout == "angie") {
                    console.log('Funciona if de : ' + rout);
                    person = 0;
                } else if (rout == "adiela") {
                    console.log('Funciona if de : ' + rout);
                    person = 1;
                } else if (rout == "uriel") {
                    console.log('Funciona if de : ' + rout);
                    person = 2;
                } else if (rout == "juan") {
                    console.log('Funciona if de : ' + rout);
                    person = 3;
                } else {


                }

                let name = people[person].names;
                console.log("name : " + name);
                let lastname = people[person].lastName;
                console.log("lastname : " + lastname);
                let address = people[person].address;
                console.log("address : " + address);
                let phone = people[person].phone;
                let email = people[person].email;

                for (let i = validacionInerpolacion.length - 1; i >= 0; i--) {
                    let value = eval(validacionInerpolacion[i]);
                    console.log(value);

                    // console.log('name :' + name);
                    // value = eval(validacionInerpolacion[p]);
                    console.log('value : ' + value);
                    htmlText = htmlText.replace("{" + validacionInerpolacion[i] + "}", value);
                    // }

                }


                // console.log('error en redfile: ' + er);
                // console.log('data: ' + data);
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.end(htmlText);
            });
        }
        // else if (!response.finished) {
        //     fs.readFile('error.html', (er, data) => {
        //         response.writeHead(404, { 'Content-Type': 'text/html' });
        //         response.end(data);
        //     })

        // }
    });
}).listen(8080);